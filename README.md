# Overcooked
¿Qué tan rápido puedes preparar una pizza? Cada ronda de juego el chef te pedirá distintos tipos de pizzas que deberás preparar con los ingredientes que correspondan. ¡Mientras más pizzas puedas preparar, mejor puntaje harás!

A este juego se le deberán agregar elementos de **Augmented Reality** (AR) cómo ejercicio. En el branch [Normal](https://gitlab.com/erojasfredini/utn.ba-vuforia-overcooked.ar/tree/Normal) se puede clonar el juego completo sin elementos de AR y en el branch [AR](https://gitlab.com/erojasfredini/utn.ba-vuforia-overcooked.ar/tree/AR) se puede clonar el juego con algunos elementos de AR.

## Requisitos
*  Unity 2019.2.3 o superior