﻿//MIT License
//
//Copyright(c) 2019 Emmanuel Rojas Fredini
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PanelPuntos : MonoBehaviour
{
	// Estrellas en la UI
    public GameObject[] estrellas;
	// Segundos en los que se muestra el resultado
	public float delay = 3.0f;
	// Panel de puntos
    public GameObject panel;
	// Mensaje de cuantos puntos se hicieron en la ronda (suma de ordenes realizadas)
    public Text mensajeFinal;

    private void Start()
    {
        mensajeFinal.enabled = false;
        Ocultar();
    }

    public void ScoreRound(int puntos)
    {
        StopAllCoroutines();
        Ocultar();
        panel.SetActive(true);

        for (int i = 0; i < puntos; ++i)
        {
            estrellas[i].SetActive(true);
        }
        StartCoroutine(OcultarConDelay());
    }

    private void Ocultar()
    {
        foreach (var estrella in estrellas)
        {
            estrella.SetActive(false);
        }
        panel.SetActive(false);
    }

    private IEnumerator OcultarConDelay()
    {
        yield return new WaitForSeconds(delay);
        Ocultar();
    }

    public void PuntajeFinal(int puntos)
    {
        StopAllCoroutines();
        Ocultar();
        mensajeFinal.enabled = true;
        mensajeFinal.text = "Hiciste " + puntos + " puntos!";
    }
}