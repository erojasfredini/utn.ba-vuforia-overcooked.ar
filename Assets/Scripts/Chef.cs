﻿//MIT License
//
//Copyright(c) 2019 Emmanuel Rojas Fredini
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
using UnityEngine.UI;
using UnityEngine;

public class Chef : MonoBehaviour
{
    public Text burbuja;

    public void NuevaOrden()
    {
        Orden orden = Manager.instancia.orden;
        if (IsCompleta(orden))
        {
            burbuja.text = "Nueva orden! \nUna completa!";
        }
        else if (IsVegetariana(orden))
        {
            burbuja.text = "Nueva orden! \nUna vegetariana!";
        }
        else if (IsCalabresa(orden))
        {
            burbuja.text = "Una calabresa.";
        }
        else if (IsChampignon(orden))
        {
            burbuja.text = "Sale una de champignones!";
        }
        else if (IsAnana(orden))
        {
            burbuja.text = "Quieren una de ananá!";
        }
        else if (IsMargarita(orden))
        {
            burbuja.text = "Ah! Una margarita!";
        }
        else if (IsSalameChampignon(orden))
        {
            burbuja.text = "Una con salame y champignon";
        }
        else if (IsSalameAnana(orden))
        {
            burbuja.text = "Sale con salame y ananá";
        }
        else
        {
            Debug.LogError("Tipo de pizza no reconocida!");
        }
    }

    private bool IsMargarita(Orden orden)
    {
        return !orden.champignon && !orden.salame && !orden.anana;
    }

    private bool IsChampignon(Orden orden)
    {
        return orden.champignon && !orden.salame && !orden.anana;
    }

    private bool IsCalabresa(Orden orden)
    {
        return !orden.champignon && orden.salame && !orden.anana;
    }

    private bool IsSalameChampignon(Orden orden)
    {
        return orden.champignon && orden.salame && !orden.anana;
    }

    private bool IsAnana(Orden orden)
    {
        return !orden.champignon && !orden.salame && orden.anana;
    }

    private bool IsVegetariana(Orden orden)
    {
        return orden.champignon && !orden.salame && orden.anana;
    }

    private bool IsSalameAnana(Orden orden)
    {
        return !orden.champignon && orden.salame && orden.anana;
    }

    private bool IsCompleta(Orden orden)
    {
        return orden.champignon && orden.salame && orden.anana;
    }
}