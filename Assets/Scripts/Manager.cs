﻿//MIT License
//
//Copyright(c) 2019 Emmanuel Rojas Fredini
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;

public struct Orden
{
    public bool salame;
    public bool anana;
    public bool champignon;
}

[System.Serializable]
public class IntEvent : UnityEvent<int> { }

public class Manager : MonoBehaviour
{
    // Segundos de juego
    public float tiempoRonda = 60.0f;

	// Elementos de la pizza
    public GameObject pizza;
    public GameObject salame;
    public GameObject champignon;
    public GameObject anana;

	// Tiempo restante (UI)
    public Text timer;

    public static Manager instancia;

	// Tiempo restante
    public static float tiempoRegresivo;
	// Puntos en orden actual
    private static int _puntos;
	// Ordenes completas por cada puntaje (ordenesPorPuntaje[i] == cuantas ordenes se completaron con i puntos)
    private static Dictionary<int, int> ordenesPorPuntaje = new Dictionary<int, int>(5);

	// Orden actual
    public Orden orden;
	// Cuando se cree una nueva orden
    public UnityEvent onNuevaOrden;
	// Cuando se complete una orden
    public IntEvent onOrdenCompleta;
	// Cuando se termine una ronda
    public IntEvent onTermino;
	// Cuando se inicie la ronda
    public UnityEvent onRestart;

    private void Start()
    {
        if (instancia == null)
        {
            instancia = this;
            Restart();
        }
        else
        {
            Destroy(this);
        }
    }

	// Iniciar una ronda
    private void Iniciar()
    {
        _puntos = 3;
        tiempoRegresivo = tiempoRonda;
        ordenesPorPuntaje.Clear();  //clear any previous scores
        ordenesPorPuntaje.Add(1, 0); //1 star scores
        ordenesPorPuntaje.Add(2, 0); //2 star scores
        ordenesPorPuntaje.Add(3, 0); //3 star scores
        ordenesPorPuntaje.Add(4, 0); //4 star scores
        ordenesPorPuntaje.Add(5, 0); //5 star scores

        NuevaOrden();
    }

    // Se completo una orden
    public void OrdenCompleta()
    {
        // Cuantos puntos hicimos?
        _puntos = 2;
        _puntos += (champignon.activeInHierarchy == orden.champignon) ?  1 : -1;
        _puntos += (salame.activeInHierarchy == orden.salame) ?  1 : -1;
        _puntos += (anana.activeInHierarchy == orden.anana) ?  1 : -1;

        // Limitemos al rango [1,5]
        _puntos = Mathf.Clamp(_puntos, 1, 5);
		if (onOrdenCompleta != null)
		{
			onOrdenCompleta.Invoke(_puntos);
		}
        Debug.Log(_puntos);
        // Agreguemos esta orden a ese listado de puntajes
        //Debug.Log(ordenesPorPuntaje.Keys.Count + " " + ordenesPorPuntaje.Values.Count);
        ordenesPorPuntaje[_puntos]++;

        // Podemos armar otra pizza?
        if (tiempoRegresivo > 0.0f)
        {
            NuevaOrden();
        }
    }

    // Comenzar una nueva orden
    public void NuevaOrden()
    {
        // Limpiamos el estado de la orden anterior
        champignon.SetActive(false);
        anana.SetActive(false);
        salame.SetActive(false);

        // Creamos una nueva orden de forma aleatoria
        orden = new Orden();
        orden.champignon = Random.value > 0.5f;
        orden.salame = Random.value > 0.5f;
        orden.anana = Random.value > 0.5f;

        onNuevaOrden.Invoke();
    }

    public void Update()
    {
        if (tiempoRegresivo > 0.0f)
        {
            tiempoRegresivo -= Time.deltaTime;
            if(timer != null)
            {
                timer.text = Mathf.RoundToInt(tiempoRegresivo).ToString();
            }
        }
        else
        {
            // Se termino el tiempo :(
            Terminar();
        }
    }

	// Terminar la ronda
    private void Terminar()
    {
        int puntosFinales = 0;

        puntosFinales += ordenesPorPuntaje[5] * 5;
        puntosFinales += ordenesPorPuntaje[4] * 4;
        puntosFinales += ordenesPorPuntaje[3] * 3;
        puntosFinales += ordenesPorPuntaje[2] * 2;
        puntosFinales += ordenesPorPuntaje[1] * 1;

		if (onTermino != null)
		{
			onTermino.Invoke(puntosFinales);
		}
    }

	// Reiniciar una nueva ronda
    public void Restart()
    {
        onRestart.Invoke();
        Iniciar();
    }

    public void PonerChampignon()
    {
        champignon.SetActiveRecursively(champignon.activeInHierarchy);
    }

    public void PonerSalame()
    {
        salame.SetActiveRecursively(salame.activeInHierarchy);
    }

    public void PonerAnana()
    {
        anana.SetActiveRecursively(anana.activeInHierarchy);
    }
}